<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Models\UserLogin;
use App\Models\User;
use Auth;
use Illuminate\Support\Arr;

class LoginController extends Controller
{
    
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    protected function redirectTo()
    {
        if (Auth::user()->roles()->first()['id']==1) {
            return RouteServiceProvider::USER;
        }
        return RouteServiceProvider::HOME;
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('throttle:10,1')->only('login'); //限制每分鐘只能呼叫10次，不然就會出限429錯誤
        $this->middleware('guest')->except('logout');
    }
    
    public function login(Request $request)
    {
        $credentials = $request->all();
        $credentials = Arr::except($credentials, ['_token','remember']);

        if (!User::where('email',$request->email)->where('state','1')->first()):
            return redirect()->to('login')->withErrors(['email'=>'state.failed']);
        endif;
        if(!Auth::validate($credentials)):
            return redirect()->to('login')->withErrors(['email'=>'auth.failed']);
        endif;

        $user = Auth::getProvider()->retrieveByCredentials($credentials);
        Auth::login($user, $request->get('remember'));
        $this->authenticated($request, $user);
        UserLogin::create(['user_id'=>Auth::user()->id,'ip_address'=>$request->ip(),'user_agent'=>$request->server('HTTP_USER_AGENT')]);
        return redirect($this->redirectTo());
    }
}
