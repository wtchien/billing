<?php
    
namespace App\Http\Controllers;
    
use App\Models\Permission;
use Illuminate\Http\Request;
    
class PermissionController extends Controller
{ 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:permission-list|permission-create|permission-edit|permission-delete', ['only' => ['index','show']]);
         $this->middleware('permission:permission-create', ['only' => ['create','store']]);
         $this->middleware('permission:permission-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:permission-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permissions = Permission::leftJoin('users', 'permissions.user_id', '=', 'users.id')->orderBy('permissions.id','DESC')
            ->select('permissions.*', 'users.name AS editor')->paginate(10);
        return view('admin.permissions.index',compact('permissions'))
            ->with('i', (request()->input('page', 1) - 1) * 10);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.permissions.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required',
            'guard_name' => 'required',
        ]);
        $input  = $request->all();
        $input['user_id'] = auth()->user()->id;
    
        Permission::create($input);
    
        return redirect()->route('permissions.index')
                        ->with('success','Permissions created successfully.');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  \App\Permissions  $permission
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $permission = Permission::find($id)->leftJoin('users AS u', 'permissions.user_id', '=', 'u.id')
            ->select('permissions.*', 'u.name AS editor')->first();
        return view('admin.permissions.show',compact('permission'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Permissions  $permission
     * @return \Illuminate\Http\Response
     */
    public function edit(Permission $permission)
    {
        return view('admin.permissions.edit',compact('permission'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Permissions  $permission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Permission $permission)
    {
        request()->validate([
            'name' => 'required',
            'guard_name' => 'required',
        ]);
        $input  = $request->all();
        $input['user_id'] = auth()->user()->id;
    
        $permission->update($input);
    
        return redirect()->route('permissions.index')
                        ->with('success','Permissions updated successfully');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Permissions  $permission
     * @return \Illuminate\Http\Response
     */
    public function destroy(Permission $permission)
    {
        $permission->delete();
    
        return redirect()->route('permissions.index')
                        ->with('success','Permissions deleted successfully');
    }
}