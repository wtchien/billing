@extends('layouts.app')


@section('content')
<div class="card p-0">
    <div class="card-body">
        <h5 class="card-title vh-25">
            <div class="float-start fs-4">
                Permission Management
            </div>
            <div class="float-end">
                <a class="btn btn-success" href="{{ route('permissions.create') }}"> Create</a>
            </div>
        </h5>
    </div>
    <div class="card-body">
        @if ($message = Session::get('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ $message }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        @endif
        <!-- Table with stripped rows -->
        <table class="table table-striped">
            <thead>
                <tr>
                    <th class="col-md-2">No</th>
                    <th class="col-md-3">Name</th>
                    <th class="col-md-3">GuardName</th>
                    <th class="col-md-2">Editor</th>
                    <th class="col-md-2">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($permissions as $permission)
        	    <tr>
        	        <td>{{ ++$i }}</td>
        	        <td>{{ $permission->name }}</td>
        	        <td>{{ $permission->guard_name }}</td>
        	        <td>{{ $permission->editor }}</td>
        	        <td>
                        <form action="{{ route('permissions.destroy',$permission->id) }}" method="POST">
                            <a class="btn btn-info" href="{{ route('permissions.show',$permission->id) }}"><i class="bi bi-eye h5"></i></a>
                            @can('permission-edit')
                            <a class="btn btn-primary" href="{{ route('permissions.edit',$permission->id) }}"><i class="bi bi-pencil-square h5"></i></a>
                            @endcan

                            @csrf
                            @method('DELETE')
                            @can('permission-delete')
                            <button type="submit" class="btn btn-danger show-alert-delete-box"><i class="bi bi-trash3 h5"></i></button>
                            @endcan
                        </form>
        	        </td>
        	    </tr>
        	    @endforeach
            </tbody>
        </table>
        <!-- End Table with stripped rows -->
    </div>
</div>

{!! $permissions->render() !!}

@endsection