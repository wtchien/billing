@extends('layouts.app')


@section('content')
<div class="container h-100">
    <div class="row d-flex justify-content-center align-items-center h-100">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>Edit Product</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-secondary" href="{{ route('products.index') }}"> Back</a>
            </div>
        </div>
        @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <div class="col-xl-9">
            <div class="card" style="border-radius: 15px;">
                <div class="card-body">
                    {!! Form::model($product, ['method' => 'PATCH','route' => ['products.update', $product->id]]) !!}
                        <div class="row align-items-center pt-2 pb-3">
                            <div class="col-md-3 ps-5">
                                <h6 class="mb-0">Name</h6>
                            </div>
                            <div class="col-md-9 pe-5">
                                {!! Form::text('name', $product->name, array('placeholder' => 'Name','class' => 'form-control form-control-lg')) !!}
                            </div>
                        </div>

                        <hr class="mx-n3">

                        <div class="row align-items-center pt-2 pb-3">
                            <div class="col-md-3 ps-5">
                                <h6 class="mb-0">Detail</h6>
                            </div>
                            <div class="col-md-9 pe-5">
                                {!! Form::textarea('detail', $product->detail, array('placeholder' => 'Detail','class' => 'form-control form-control-lg')) !!}
                            </div>
                        </div>

                        <hr class="mx-n3">

                        <div class="px-5 py-4 text-center">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection