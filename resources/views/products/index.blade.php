@extends('layouts.app')


@section('content')
<div class="card p-0">
    <div class="card-body">
        <h5 class="card-title vh-25">
            <div class="float-start fs-4">
                Products
            </div>
            <div class="float-end">
                <a class="btn btn-success" href="{{ route('products.create') }}"> Create</a>
            </div>
        </h5>
    </div>
    <div class="card-body">
        @if ($message = Session::get('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ $message }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        @endif
        <!-- Table with stripped rows -->
        <table class="table table-striped">
            <thead>
                <tr>
                    <th class="col-md-1">No</th>
                    <th class="col-md-4">Name</th>
                    <th class="col-md-4">Details</th>
                    <th class="col-md-3">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($products as $product)
        	    <tr>
        	        <td>{{ ++$i }}</td>
        	        <td>{{ Str::limit($product->name,24) }}</td>
        	        <td>{{ Str::limit($product->detail,30) }}</td>
        	        <td>
                        <form action="{{ route('products.destroy',$product->id) }}" method="POST">
                            <a class="btn btn-info" href="{{ route('products.show',$product->id) }}"><i class="bi bi-eye h5"></i></a>
                            @can('product-edit')
                            <a class="btn btn-primary" href="{{ route('products.edit',$product->id) }}"><i class="bi bi-pencil-square h5"></i></a>
                            @endcan


                            @csrf
                            @method('DELETE')
                            @can('product-delete')
                            <button type="submit" class="btn btn-danger show-alert-delete-box"><i class="bi bi-trash3 h5"></i></button>
                            @endcan
                        </form>
        	        </td>
        	    </tr>
        	    @endforeach
            </tbody>
        </table>
        <!-- End Table with stripped rows -->

    </div>
</div>

{!! $products->render() !!}

@endsection