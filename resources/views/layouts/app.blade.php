@php 
    $route  = request()->route()->uri;
@endphp
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Billing') }}</title>

    <!-- Scripts -->
    <script http-equiv="x-pjax-version" src="{{ mix('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="//fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link http-equiv="x-pjax-version" href="{{ mix('css/app.css') }}" rel="stylesheet">
    <link href="//cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ asset('NiceAdmin/assets/vendor/bootstrap-icons/bootstrap-icons.css') }}" rel="stylesheet">
    <link href="//cdn.datatables.net/1.13.4/css/dataTables.bootstrap5.min.css" rel="stylesheet">
    <link href="//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/5.0.7/sweetalert2.min.css" rel="stylesheet">
    <link href="{{ asset('NiceAdmin/assets/css/style.css') }}" rel="stylesheet">
    <link href="//cdn.jsdelivr.net/npm/jquery-datetimepicker@2.5.21/jquery.datetimepicker.min.css" rel="stylesheet">
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
</head>
<body>
    <div id="app">
        <!-- ======= Header ======= -->
        <header id="header" class="header fixed-top d-flex align-items-center">

            <div class="d-flex align-items-center justify-content-between">
                <a class="navbar-brand" href="{{ url('/') }}">
                    &#x60A0;&#x9054;&#x5929;&#x4E0B; | Yotta - Billing
                </a>
                <i class="bi bi-list toggle-sidebar-btn"></i>
            </div><!-- End Logo -->

            <nav class="header-nav ms-auto">
            @if(Auth::check())
                <ul class="d-flex align-items-center">
                    <li class="nav-item dropdown pe-3">
                        <a class="nav-link nav-profile d-flex align-items-center pe-0" href="#" data-bs-toggle="dropdown">
                            <span class="d-none d-md-block dropdown-toggle ps-2">{{ Auth::user()->name }}</span>
                        </a><!-- End Profile Iamge Icon -->

                        <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow profile">
                            <li>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </li>

                        </ul><!-- End Profile Dropdown Items -->
                    </li><!-- End Profile Nav -->
                </ul>
            @endif
            </nav><!-- End Icons Navigation -->

        </header><!-- End Header -->

        <!-- ======= Sidebar ======= -->
        <aside id="sidebar" class="sidebar">

            <ul class="sidebar-nav" id="sidebar-nav">

                @guest
                    @if (Route::has('login'))
                        <li class="nav-item">
                            <a class="nav-link collapsed" href="{{ route('login') }}">
                                <i class="bi bi-door-open-fill"></i>
                                <span>{{ __('Login') }}</span>
                            </a>
                        </li>
                    @endif

                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link collapsed" href="{{ route('register') }}">
                                <i class="bi bi-person-fill-add"></i>
                                <span>{{ __('Register') }}</span>
                            </a>
                        </li>
                    @endif
                @else
                    <li class="nav-item">
                        <a class="nav-link @if (strpos($route, 'products')===false)collapsed @endif" href="{{ route('products.index') }}">
                            <i class="bi bi-grid"></i>
                            <span>Product</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @if ((strpos($route, 'users') || strpos($route, 'roles') || strpos($route, 'permissions'))===false) collapsed @endif" data-bs-target="#mgmts-nav" data-bs-toggle="collapse" href="#">
                            <i class="bi bi-person-fill-gear"></i><span>Mgmt</span><i class="bi bi-chevron-down ms-auto"></i>
                        </a>
                        <ul id="mgmts-nav" class="nav-content collapse @if ((strpos($route, 'users') || strpos($route, 'roles') || strpos($route, 'permissions'))===true) show @endif" data-bs-parent="#sidebar-nav">
                            <li>
                                <a href="{{ route('users.index') }}" @if (strpos($route, 'users')==true)class="active"@endif>
                                    <i class="bi bi-circle"></i><span>Users</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('roles.index') }}" @if (strpos($route, 'roles')==true)class="active"@endif>
                                    <i class="bi bi-circle"></i><span>Role</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('permissions.index') }}" @if (strpos($route, 'permissions')==true)class="active"@endif>
                                    <i class="bi bi-circle"></i><span>Permission</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                @endguest

        </aside><!-- End Sidebar-->

        <main id="main" class="main">

            <section class="section">
                <div class="row">
                @yield('content')
                </div>
            </section>

        </main><!-- End #main -->

        <!-- ======= Footer ======= -->
        <footer id="footer" class="footer">
            <div class="copyright">
                &copy; Copyright <strong><span>悠達天下</span></strong>. All Rights Reserved
            </div>
            <div class="credits">
                <!-- All the links in the footer should remain intact. -->
                <!-- You can delete the links only if you purchased the pro version. -->
                <!-- Licensing information: //bootstrapmade.com/license/ -->
                <!-- Purchase the pro version with working PHP/AJAX contact form: //bootstrapmade.com/nice-admin-bootstrap-admin-html-template/ -->
                Designed by <a href="//bootstrapmade.com/">BootstrapMade</a>
            </div>
        </footer>
    </div>
    <script src="//cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/jquery-datetimepicker@2.5.21/build/jquery.datetimepicker.full.min.js"></script>
    <script src="//cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/1.13.4/js/dataTables.bootstrap5.min.js"></script>
    <script src="{{ asset('NiceAdmin/assets/vendor/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('NiceAdmin/assets/js/main.js') }}"></script>
    <script type="text/javascript">
        let opt = {
            oLanguage: {
                url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Chinese-traditional.json" 
            },
            "columnDefs": [{
                "targets": '_all',
                "createdCell": function(td, cellData, rowData, row, col) {
                    $(td).css('padding', '3px')
                }
            }],
            "lengthMenu": [
                [10, 20, 50, -1],
                [10, 20, 50, "All"]
            ],
            "order": [
                [0, 'desc']
            ],
            rowReorder: {
                selector: 'td:nth-child(8)'
            },
            scrollY: true,
            scrollCollapse: true,
            scroller: {
                loadingIndicator: true
            },
            responsive: true,
            deferRender: true,
            rendering: true,
            scroller: true,
            retrieve: true,
            processing: true,
            autoWidth: false,
        }
    
        $('.show-alert-delete-box').click(function(event){
            var form =  $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                title: "Are you sure you want to delete this record?",
                text: "If you delete this, it will be gone forever.",
                icon: "warning",
                type: "warning",
                buttons: ["Cancel","Yes!"],
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((willDelete) => {
                if (willDelete) {
                    form.submit();
                }
            });
        });
        $( function() {
            $.datetimepicker.setLocale('zh-TW');
            $( ".datetime" ).datetimepicker({
                format:'Y-m-d H:i',
                step:30,
                minTime:'07:00',
                maxTime:'21:00'
            });
        });
    </script>
</body>
</html>
